import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const ResetPassword = ({ navigation }) => {

    // 57%
    return (
        <View>
            <ScrollView>
                <View style={{ height: hp("30%",), width: wp('100%'), backgroundColor: "black", alignItems: "center" }}>

                    <Image source={require("../../Assets/logo.png")} style={{ height: hp("20%"), width: wp("70%"), marginTop: 40 }}></Image>
                </View>
                <View style={{ marginTop: 40 }}>






                    <Input
                        secureTextEntry={true}
                        placeholder=' New Password'
                        leftIcon={
                            <Icon
                                name='lock'
                                size={24}
                                color='black'
                            />
                        }
                        containerStyle={{ width: wp("90%"), alignSelf: "center" }}
                        style={{ fontSize: 14, }}
                    />

                    <Input
                        secureTextEntry={true}
                        placeholder=' Confrim Password'
                        leftIcon={
                            <Icon
                                name='lock'
                                size={24}
                                color='black'
                            />
                        }
                        containerStyle={{ width: wp("90%"), alignSelf: "center" }}
                        style={{ fontSize: 14, }}
                    />
                    <Button
                        onPress={() => navigation.navigate("Login")}
                        buttonStyle={{ width: wp("90%"), height: hp("8%"), borderColor: "black", borderWidth: 1, alignSelf: "center", borderRadius: 10, marginTop: 70, backgroundColor: "black", marginBottom: 20 }}
                        // raised={true}
                        titleStyle={{ color: "white" }}
                        title="Submit"
                        type="solid"
                    />

                </View>
            </ScrollView>
        </View>
    );
};

export default ResetPassword;
