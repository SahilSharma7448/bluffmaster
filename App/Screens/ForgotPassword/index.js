
import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, Alert } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../Utility';
import * as Utility from '../../Utility';
import { useDispatch } from 'react-redux';
import {
    forgotPasswordApi,

} from '../../Redux/Action';
import Loader from '../../Constants/Loader';
import Logo from "../../Constants/Logo"

const ForgotPassword = ({ navigation }) => {
    const dispatch = useDispatch()
    const [email, setEmail] = useState('');
    const [loader, setLoader] = useState(false);

    // 57%
    const forgotFu = async () => {
        if (await Utility.isFieldEmpty(email)) {
            return Alert.alert('Please Fill Email');
        } else if (await Utility.isValidEmail(email)) {
            return Alert.alert('Please Fill Valid Email');
        } else {
            setLoader(true)
            // navigation.navigate('OtpVerification');
            let res = await dispatch(forgotPasswordApi(email.toLocaleLowerCase()))
            if (res.status == 1) {
                setLoader(false)

                Alert.alert(
                    ' ',
                    'Email send. Check your gmail',
                    [{ text: 'ok', onPress: () => navigation.navigate('Login') }],
                    { cancelable: false },
                );
                return true;
            }
            else {
                setLoader(false)
                return Alert.alert('', res.message);
            }
        }
    };
    return (
        <View>
            <Loader isLoading={loader}></Loader>

            <ScrollView>
                {/* <View style={styles.backgroundView}>
                    <Image
                        source={require('../../Assets/logo.png')}
                        style={styles.logoImg}></Image>
                </View> */}
                <Logo></Logo>
                <Text style={styles.forgotTxt}>Forgot your password</Text>

                <View style={{ marginTop: 70 }}>
                    <Input
                        onChangeText={(text) => setEmail(text)}
                        placeholder="Your Email Adress"
                        leftIcon={<Icon name="envelope" size={17} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />

                    <Button
                        onPress={() => forgotFu()}
                        buttonStyle={styles.otpButton}
                        // raised={true}
                        titleStyle={{ color: 'white' }}
                        title="Submit"
                        type="solid"
                    />
                </View>
            </ScrollView>
        </View>
    );
};
export default ForgotPassword;
const styles = StyleSheet.create({
    backgroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: {
        height: hp('20%'),
        width: wp('70%'),
        marginTop: 40,
    },
    forgotTxt: {
        fontSize: 22,
        fontWeight: '600',
        textAlign: 'center',
        marginTop: 30,
    },
    inputContainer: { width: wp('90%'), alignSelf: 'center' },
    otpButton: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 70,
        backgroundColor: 'black',
        marginBottom: 20,
    },
});