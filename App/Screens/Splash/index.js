import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
import * as Utility from "../../Utility/index"
import { useDispatch } from 'react-redux';
import {
  getRecentData,
  getBanners,
  getCategroy
} from '../../Redux/Action';
const Splash = ({ navigation }) => {
  const dispatch = useDispatch()
  useEffect(async () => {
    await dispatch(getRecentData())
    await dispatch(getBanners())
    await dispatch(getCategroy())
    timeoutHandle = setTimeout(() => {
      retrieveData();
    }, 2000);
  });
  const retrieveData = async () => {
    let token = await Utility.getFromLocalStorge('token')
    if (token == null) {
      navigation.navigate("Login")

    }
    else {


      navigation.navigate("BottomTab")

    }

  };

  return (

    <View style={{ height: hp("100%"), width: wp("100%"), alignItems: "center", justifyContent: "center", backgroundColor: "black" }}>
      <Image source={require("../../Assets/logoNew.png")} style={{ height: hp("20%"), width: wp("70%") }}></Image>
      <Text style={{ color: "white", textAlign: "center", marginTop: 20, fontSize: 12 }}>Explore the best place to buy and</Text>
      <Text style={{ color: "white", textAlign: "center", fontSize: 12 }}>Sell Prdoucts</Text>

    </View>
  );
};

export default Splash;

