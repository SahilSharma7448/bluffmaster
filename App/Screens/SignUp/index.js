
import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, Alert } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../Utility';
import * as Utility from '../../Utility';
import { useDispatch } from 'react-redux';
import {
    registerApi,

} from '../../Redux/Action';
import Loader from '../../Constants/Loader';
import Logo from "../../Constants/Logo"
import {
    loginApi,
    getBanners,
    getCategroy
} from '../../Redux/Action';
const SignUp = ({ navigation }) => {
    const dispatch = useDispatch()
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phoneNo, setPhoneNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loader, setLoader] = useState(false);

    const registerFu = async () => {
        console.log("check ", firstName, "last", lastName, "email", email, "pass", password)
        if (
            await Utility.isFieldEmpty(firstName && lastName && phoneNo && password,)
        ) {
            return Alert.alert('Please Fill All Fields');
        }
        //  else if (await Utility.isValidPhoneNumber(phoneNo)) {
        //     return Alert.alert('Please fill valid Phone No');
        // } 
        else {
            // navigation.navigate('Login');
            setLoader(true)

            let res = await dispatch(registerApi(firstName, lastName, email.toLocaleLowerCase(), password.toLocaleLowerCase(), phoneNo))
            if (res.status == 1) {
                setLoader(false)

                Alert.alert(
                    ' ',
                    'User Register Successfully',
                    [{ text: 'ok', onPress: () => loginData() }],
                    { cancelable: false },
                );
                return true;
            }
            else {
                setLoader(false)
                return Alert.alert('', res.message);
            }
        }
    };
    const loginData = async () => {
        // navigation.navigate('Login')
        setLoader(true)
        let res = await dispatch(loginApi(phoneNo, password))
        if (res.status == 1) {
            setLoader(false)

            console.log("check if")
            navigation.navigate('BottomTab');
        }
        else {
            console.log("check else")
            setLoader(false)

            return Alert.alert('', res.message);
        }
    }
    // 57%
    return (
        <View>
            <ScrollView>
                <Loader isLoading={loader}></Loader>

                {/* <View style={styles.backgroundView}>
                    <Image
                        source={require('../../Assets/logo.png')}
                        style={styles.logoImg}></Image>
                </View> */}
                <Logo></Logo>
                <View style={{ marginTop: 40 }}>
                    <Input
                        onChangeText={(text) => setFirstName(text)}
                        placeholder="First Name"
                        leftIcon={<Icon name="user" size={20} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    <Input
                        onChangeText={(text) => setLastName(text)}
                        placeholder="Last Name"
                        leftIcon={<Icon name="user" size={20} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    {/* <Input
                        onChangeTex={(text) => setPhoneNo(text)}
                        placeholder="Phone No"
                        keyboardType={'number-pad'}
                        leftIcon={<Icon name="mobile" size={23} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    /> */}

                    <Input
                        onChangeText={(text) => setEmail(text)}
                        placeholder="Your Email Adress"
                        leftIcon={<Icon name="envelope" size={17} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    <Input
                        keyboardType={'number-pad'}
                        onChangeText={(text) => setPhoneNo(text)}
                        placeholder="Your PhoneNo"
                        leftIcon={<Icon name="phone" size={17} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    <Input
                        onChangeText={(text) => setPassword(text)}
                        secureTextEntry={true}
                        placeholder="Your Password"
                        leftIcon={<Icon name="lock" size={24} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />

                    <Button
                        onPress={() => registerFu()}
                        buttonStyle={styles.RegisterButton}
                        // raised={true}
                        titleStyle={{ color: 'white' }}
                        title="Register"
                        type="solid"
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default SignUp;

const styles = StyleSheet.create({
    backgroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: {
        height: hp('20%'),
        width: wp('70%'),
        marginTop: 40,
    },
    inputContainer: { width: wp('90%'), alignSelf: 'center' },
    RegisterButton: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        backgroundColor: 'black',
        marginBottom: 20,
    },
});
