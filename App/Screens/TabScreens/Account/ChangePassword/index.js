
import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, Alert } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../../Utility';
import * as Utility from '../../../../Utility';
import { useDispatch, useSelector } from 'react-redux';
import {
    changePasswordApi,

} from '../../../../Redux/Action';
import Loader from '../../../../Constants/Loader';
import Logo from "../../../../Constants/Logo"

const ChangePassword = ({ navigation }) => {
    const dispatch = useDispatch()
    const [newPass, setNewPass] = useState('');
    const [confrimPass, setConfrimPass] = useState('');
    const [loader, setLoader] = useState(false);

    // 57%
    const submitFn = async () => {
        if (await Utility.isFieldEmpty(newPass, confrimPass)) {
            return Alert.alert('Please fill All Fields');
        }

        else {
            // navigation.navigate('Login');
            setLoader(true)
            let res = await dispatch(changePasswordApi(newPass.toLocaleLowerCase(), confrimPass.toLocaleLowerCase()));
            if (res.status == 1) {
                setLoader(false)
                Alert.alert(
                    'Password Change ',
                    'Please Login Again',
                    [{ text: 'ok', onPress: () => navigation.navigate('Login') }],
                    { cancelable: false },
                );
                return true;
            }
            else {
                setLoader(false);
                return Alert.alert('', res.message);
            }
        }
    };
    return (
        <View>
            <Loader isLoading={loader}></Loader>

            <ScrollView>
                <Logo></Logo>
                <View style={{ marginTop: 40 }}>
                    <Input
                        onChangeText={(text) => setNewPass(text)}
                        secureTextEntry={true}
                        placeholder="Old Password"
                        leftIcon={<Icon name="lock" size={24} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />

                    <Input
                        onChangeText={(text) => setConfrimPass(text)}
                        secureTextEntry={true}
                        placeholder="New Password"
                        leftIcon={<Icon name="lock" size={24} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    <Button
                        onPress={() => submitFn()}
                        buttonStyle={styles.submitBtn}
                        // raised={true}
                        titleStyle={{ color: 'white' }}
                        title="Submit"
                        type="solid"
                    />
                </View>
            </ScrollView>
        </View>
    );
};

export default ChangePassword;
const styles = StyleSheet.create({
    backgroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: {
        height: hp('20%'),
        width: wp('70%'),
        marginTop: 40,
    },
    inputContainer: { width: wp('90%'), alignSelf: 'center' },
    submitBtn: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 70,
        backgroundColor: 'black',
        marginBottom: 20,
    },
});