import React, { useEffect } from 'react';
import { View, Text, Image, ImageBackground, ScrollView } from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../../Utility';
const ContantUs = ({ navigation }) => {
    return (
        <View>

            <View style={{ backgroundColor: 'white', width: '100%', height: '100%' }}>
                <View style={{ alignItems: 'center', marginTop: '13%' }}>


                    <Text
                        style={{
                            fontWeight: 'bold',
                            color: 'black',
                            fontSize: hp('3.2'),
                        }}>
                        CONTACT US
          </Text>
                </View>
                <View
                    style={{
                        backgroundColor: 'black',
                        width: '100%',
                        height: '60%',
                        bottom: 0,
                        position: 'absolute',
                        borderTopRightRadius: 40,
                        borderTopLeftRadius: 40,
                    }}>
                    <View
                        style={{
                            top: 10,
                            left: 10,
                            paddingRight: '5%',
                            marginBottom: '10%',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                        <Text
                            style={{
                                color: 'white',
                                marginTop: '15%',
                                fontWeight: 'bold',
                                color: 'white',
                                fontSize: hp('2.8'),
                            }}>
                            Head Office{' '}
                        </Text>
                        <Text
                            style={{
                                color: 'white',
                                marginTop: '3%',
                                lineHeight: 25,
                                fontSize: hp('2.3'),
                            }}>
                            #Booth no 189,{'\n'}Patel market
              Sector 15 d,{'\n'}Chandigarh  bluffmaster
            </Text>
                        <Text
                            style={{
                                color: 'white',
                                marginTop: '3%',
                                fontWeight: 'bold',
                                fontSize: hp('2.3'),
                            }}>
                            PIN : 160015
            </Text>
                        <Text
                            style={{
                                color: 'white',
                                marginTop: '3%',
                                fontWeight: 'bold',
                                fontSize: hp('2.0'),
                            }}>
                            E-mail :bluffmaster@gmail.com
            </Text>
                        <Text
                            style={{
                                color: 'white',
                                marginTop: '3%',
                                fontWeight: 'bold',
                                fontSize: hp('2.0'),
                            }}>
                            Phone no : +91-9888977999
            </Text>
                    </View>
                </View>
            </View>
        </View>
    );
};
export default ContantUs;
