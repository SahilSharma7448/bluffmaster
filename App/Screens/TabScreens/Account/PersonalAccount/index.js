
import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../../../Utility';
import * as Utility from '../../../../Utility';
import { useDispatch, useSelector } from 'react-redux';
import {
    registerApi,

} from '../../../../Redux/Action';
import Loader from '../../../../Constants/Loader';
import Logo from "../../../../Constants/Logo"

const PersonalAccount = ({ navigation }) => {
    const dispatch = useDispatch()
    const userList = useSelector((state) => state.MyProfileData);
    console.log("check my data???????", userList)
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phoneNo, setPhoneNo] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loader, setLoader] = useState(false);

    const registerFu = async () => {
        console.log("check ", firstName, "last", lastName, "email", email, "pass", password)
        if (
            await Utility.isFieldEmpty(firstName, lastName, email, password)
        ) {
            return Alert.alert('Please Fill All Fields');
        }
        //  else if (await Utility.isValidPhoneNumber(phoneNo)) {
        //     return Alert.alert('Please fill valid Phone No');
        // } 
        else if (await Utility.isValidEmail(email)) {
            return Alert.alert('Please fill valid email');
        } else {
            // navigation.navigate('Login');
            setLoader(true)

            let res = await dispatch(registerApi(firstName, lastName, email.toLocaleLowerCase(), password.toLocaleLowerCase()))
            if (res.status == 1) {
                setLoader(false)

                Alert.alert(
                    ' ',
                    'User Register Successfully',
                    [{ text: 'ok', onPress: () => navigation.navigate('Login') }],
                    { cancelable: false },
                );
                return true;
            }
            else {
                setLoader(false)
                return Alert.alert('', res.message);
            }
        }
    };
    // 57%
    return (
        <View>
            <ScrollView>
                <Loader isLoading={loader}></Loader>
                <Logo></Logo>
                <View style={{ marginTop: 100 }}>
                    <Input
                        onChangeText={(text) => setFirstName(text)}
                        placeholder="Name"
                        leftIcon={<Icon name="user" size={20} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                        value={userList.name}
                        editable={false}
                    />
                    {/* <Input
                        onChangeText={(text) => setLastName(text)}
                        placeholder="Last Name"
                        leftIcon={<Icon name="user" size={20} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    /> */}
                    {/* <Input
                        onChangeTex={(text) => setPhoneNo(text)}
                        placeholder="Phone No"
                        keyboardType={'number-pad'}
                        leftIcon={<Icon name="mobile" size={23} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    /> */}

                    <Input
                        onChangeText={(text) => setEmail(text)}
                        placeholder="Your Email Adress"
                        leftIcon={<Icon name="envelope" size={17} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                        value={userList.email}
                        editable={false}

                    />

                    <TouchableOpacity onPress={() => navigation.navigate('ChangePassword')}>
                        <View style={{
                            width: wp("90%"), shadowColor: '#000',
                            shadowOffset: { width: 0, height: 1 },
                            shadowOpacity: 0.8,
                            shadowRadius: 2,
                            elevation: 5,
                            backgroundColor: "white",
                            alignItems: "center",
                            justifyContent: "space-between",
                            marginBottom: 10,
                            alignSelf: "center",
                            marginTop: 10,
                            flexDirection: "row",
                            paddingLeft: 10, paddingRight: 10,
                            height: hp("8%")

                        }}>

                            <Text >{'change password'}</Text>
                            <Icon

                                name='chevron-right'
                                size={20}
                                color='black'
                            />

                        </View>
                    </TouchableOpacity>
                    {/* <Button
                        onPress={() => registerFu()}
                        buttonStyle={styles.RegisterButton}
                        // raised={true}
                        titleStyle={{ color: 'white' }}
                        title="Register"
                        type="solid"
                    /> */}
                </View>
            </ScrollView>
        </View>
    );
};

export default PersonalAccount;

const styles = StyleSheet.create({
    backgroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: {
        height: hp('20%'),
        width: wp('70%'),
        marginTop: 40,
    },
    inputContainer: { width: wp('90%'), alignSelf: 'center' },
    RegisterButton: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        backgroundColor: 'black',
        marginBottom: 20,
    },
});
