import React, { useState } from 'react';
import { View, Text, FlatList, ScrollView, Image } from 'react-native';
import { heightPercentageToDP, widthPercentageToDP } from '../../../../Utility';
import { WebView, WebViewMessageEvent } from 'react-native-webview';

const PrivacyPolicy = ({ navigation }) => {
    return (
        <View style={{ marginBottom: 100, flex: 1 }}>

            <WebView
                style={{
                    height: heightPercentageToDP('80%'),
                    width: widthPercentageToDP('90%'),
                    alignSelf: 'center',
                }}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{
                    uri: 'http://65.0.93.145/bluff/public/privacy',
                }}
                originWhitelist={['*']}
            />
        </View>
    );
};
export default PrivacyPolicy;
