import React, { useEffect } from 'react';
import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, FlatList } from 'react-native';
import { SearchBar, Input } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../../Utility"
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch } from 'react-redux';
import {
    getOwnProfile,

} from '../../../../Redux/Action';
import Loader from '../../../../Constants/Loader';
import { Alert } from 'react-native';
import * as Utility from '../../../../Utility';

const AccountHome = ({ navigation }) => {
    const dispatch = useDispatch()
    const [searchText, setSearctText] = useState("")
    const [loader, setLoader] = useState(false);

    // 57%

    const recentData = [
        { name: "Account", route: 'PersonalAccount' },
        { name: "Contact Us", route: 'ContactUs' },
        { name: "Privacy Policy", route: 'PrivacyPolicy' },
        { name: "FeedBack", route: '' }
    ]
    const getProfile = async (item) => {
        if (item.route == 'PersonalAccount') {
            setLoader(true)
            let res = await dispatch(getOwnProfile())
            if (res.status == 1) {
                setLoader(false)
                navigation.navigate("PersonalAccount");
            }
            else {
                setLoader(false)

                return Alert.alert('', res.message)
            }
        }
        else {
            navigation.navigate(item.route)
        }

    }
    const logOut = () => {
        Alert.alert(
            '',
            'Are you logout',
            [
                { text: 'No', onPress: () => { }, style: 'cancel' },
                { text: 'Yes', onPress: () => removeAll() },
            ],

            { cancelable: false },
        );
        return true;
    };
    const removeAll = async () => {
        await Utility.removeAuthKey('token'),
            navigation.navigate('Login');
    };
    return (
        <View>
            <Loader isLoading={loader}></Loader>

            <ScrollView>
                <View style={{ marginTop: 70 }}>

                </View>

                <FlatList
                    data={recentData}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => getProfile(item)}>
                            <View style={{
                                width: wp("90%"), shadowColor: '#000',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 5,
                                backgroundColor: "white",
                                alignItems: "center",
                                justifyContent: "space-between",
                                marginBottom: 10,
                                alignSelf: "center",
                                marginTop: 10,
                                flexDirection: "row",
                                paddingLeft: 10, paddingRight: 10,
                                height: hp("8%")

                            }}>

                                <Text >{item.name}</Text>
                                <Icon

                                    name='chevron-right'
                                    size={20}
                                    color='black'
                                />

                            </View>
                        </TouchableOpacity>
                    }

                />

                <Text style={{ marginTop: 70, textAlign: "center", color: "red", fontSize: 20, fontWeight: "bold" }} onPress={() => logOut()}>Logout</Text>

            </ScrollView>
        </View >
    );
};

export default AccountHome;


