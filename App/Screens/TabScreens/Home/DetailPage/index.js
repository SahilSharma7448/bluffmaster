import React, { useEffect, useRef } from 'react';
import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, Linking, Platform } from 'react-native';
import { SearchBar, Input } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../../Utility"
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import MainHeader from "../../../../Components/MainHeader"
import { useDispatch, useSelector } from 'react-redux';
import {
    likeUnlike

} from '../../../../Redux/Action';
import FastImage from 'react-native-fast-image';

const DetailPage = ({ navigation }) => {
    const productDetail = useSelector((state) => state.GetProductDetail);
    const dispatch = useDispatch();
    // 57%
    const ProductsImageRef = useRef("")
    const [activeSlide, setActiveSlide] = useState(0)
    const [like, setLike] = useState(false)

    const _renderItem = ({ item, index }) => {
        return (
            // <Image source={{ uri: item.image }} ></Image>
            <FastImage
                source={{ uri: item.image }} style={{ height: hp("40%"), width: wp("90%"), resizeMode: "cover" }}
                resizeMode={FastImage.resizeMode.contain}
            />

        );
    }
    const openDialScreen = () => {
        let number = '';
        if (Platform.OS === 'ios') {
            number = 'telprompt:${"+91-9888977999"}';
        }
        else {
            number = 'tel:${"+91-9888977999"}';
        }
        Linking.openURL(number);
    };
    const likeClick = async () => {
        await setLike(!like)
        console.log(productDetail.id)
        await dispatch(likeUnlike(productDetail.id))
    }
    return (
        <View style={{ flex: 1 }}>

            <MainHeader navigation={navigation} name={productDetail.name}></MainHeader>
            <ScrollView>

                <Carousel
                    style={{ backgroundColor: "red" }}

                    containerCustomStyle={{ backgroundColor: "grey", alignSelf: "center", paddingTop: 20 }}
                    // layout={'stack'} layoutCardOffset={`18`}
                    ref={ProductsImageRef}
                    data={productDetail.images}
                    renderItem={_renderItem}
                    sliderWidth={wp("100%")}
                    itemWidth={wp("90%")}
                    onSnapToItem={(index) => setActiveSlide(index)}
                />
                <Pagination
                    dotsLength={productDetail.images.length}
                    activeDotIndex={activeSlide}
                    containerStyle={{ backgroundColor: 'grey' }}
                    dotStyle={{
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginHorizontal: 8,
                        backgroundColor: 'rgba(255, 255, 255, 0.92)'
                    }}
                    inactiveDotStyle={{
                        // Define styles for inactive dots here
                    }}
                    inactiveDotOpacity={0.4}
                    inactiveDotScale={0.6}
                />
                {/* <TouchableOpacity onPress={() => setLike(!like)}>
                    <View style={{ height: hp("7%"), width: wp(" 53%"), borderColor: "black", borderWidth: 1, alignSelf: "center", alignItems: "center", flexDirection: "row", justifyContent: "space-evenly", marginTop: 20, borderRadius: 10 }}>
                        <View style={{ width: wp("7%"), }}>
                            {like == false ?
                                <Image source={require("../../../../Assets/wishList.png")} style={{ height: 20, width: 20 }}></Image>
                                : <Image source={require("../../../../Assets/RedHeart.png")} style={{ height: 20, width: 20 }}></Image>
                            }
                        </View>
                        <Text style={{ fontSize: 12, width: ("73%"), }}>{like == false ? "Add" : "Remove"} to favorite</Text>
                    </View>
                </TouchableOpacity> */}
                <Text style={{ fontWeight: "700", fontSize: 17, marginTop: 10, marginLeft: 20 }}>{productDetail.name}</Text>
                <View style={{ flexDirection: "row", marginLeft: 20, paddingTop: 10 }}>
                    <TouchableOpacity onPress={() => likeClick()}>
                        <Icon
                            name='heart'
                            size={20}
                            color={like == true || productDetail.isLiked == '1' ? "red" : 'grey'}
                        />
                    </TouchableOpacity>
                    <Text style={{ marginLeft: 7, bottom: 0 }}>{productDetail.likeCount} likes</Text>
                </View>

                <View style={{ height: hp('8%'), flexDirection: 'row', backgroundColor: "black", marginTop: 10, marginBottom: 20, alignItems: "center", paddingLeft: 10, paddingRight: 10, justifyContent: "center" }}>
                    <Icon name="mobile" size={27} color="white" />
                    <Text
                        onPress={() => openDialScreen()}
                        style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: hp('1.7'),
                            marginLeft: 10,
                            textDecorationLine: "underline"
                        }}>
                        +91-9888977999
            </Text>

                    <Icon name="envelope" size={17} color="white" style={{ marginLeft: 17 }} />
                    <Text
                        onPress={() => Linking.openURL('mailto:bluffmaster@gmail.com')}
                        style={{
                            color: 'white',
                            fontWeight: 'bold',
                            fontSize: hp('1.8'),
                            marginLeft: 10
                        }}>
                        bluffmaster@gmail.com
            </Text>
                </View>

                <Text style={{ marginLeft: 20, marginBottom: 20 }}>{productDetail.description}</Text>
            </ScrollView>

        </View >
    );
};

export default DetailPage;


