import React, { useEffect, useRef } from 'react';
import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, FlatList, RefreshControl, } from 'react-native';
import { SearchBar, Input } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../../Utility"
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch, useSelector } from 'react-redux';
import {
    getProductDetail,
    searchProduct
} from '../../../../Redux/Action';
import Loader from '../../../../Constants/Loader';

const ProductPage = ({ navigation }) => {
    const dispatch = useDispatch()
    const productsList = useSelector((state) => state.GetProducts);
    const searchData = useSelector((state) => state.SearchProductData);
    console.log("product search data", searchData)

    const [SearchTxt, setSearchTxt] = useState('')
    const [showSearchData, setshowSearchData] = useState('hide')
    const [refresh, setRefresh] = useState(false);
    const [loader, setLoader] = useState(false);

    const getDetail = async (item) => {
        await dispatch(getProductDetail(item))
        navigation.navigate('DetailPage')


    }
    const searchClick = async () => {
        console.log("check ")
        Keyboard.dismiss
        setLoader(true)
        let res = await dispatch(searchProduct(SearchTxt));
        if (res.status == 1) {
            setshowSearchData('show')
            setLoader(false)
        }
        else {
            setLoader(false)

        }
    }
    const pullToRefresh = () => {
        setRefresh(true);
        setshowSearchData('hide')
        // dispatch(GetHomeListing());
        // console.log('check funcation');
        setRefresh(false);
    }
    return (
        <View>
            <Loader isLoading={loader}></Loader>

            <ScrollView
                refreshControl={
                    <RefreshControl
                        colors={['#9Bd35A', '#689F38']}
                        refreshing={refresh}
                        onRefresh={() => pullToRefresh()}
                    />
                }
            >
                <View style={{ marginTop: 20 }}>


                    <Input
                        onChangeText={(txt) => setSearchTxt(txt)}
                        onBlur={() => searchClick()}
                        keyboardAppearance={"dark"}
                        placeholder="Type here to search"
                        rightIcon={
                            <TouchableWithoutFeedback onPress={() => searchClick()}>
                                <Icon
                                    name='search'
                                    size={17}
                                    color='grey'
                                />
                            </TouchableWithoutFeedback>
                        }

                        inputContainerStyle={{ backgroundColor: "white", borderWidth: 1, borderColor: "grey", borderRadius: 7, paddingRight: 10, paddingLeft: 10 }}
                        containerStyle={{ width: wp("95%"), alignSelf: "center", }}
                        style={{ fontSize: 14, }}
                    />
                </View>
                {showSearchData == 'hide' ?
                    <FlatList
                        data={productsList}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => getDetail(item)}>
                                <View style={{
                                    width: wp("90%"), shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    elevation: 5,
                                    backgroundColor: "white",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    marginBottom: 10,
                                    alignSelf: "center",
                                    marginTop: 10,
                                    flexDirection: "row",
                                    paddingLeft: 10, paddingRight: 10

                                }}>
                                    <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}>
                                        <View style={{ width: 49, alignItems: "center", justifyContent: "center" }}>
                                            <Image source={{ uri: item.poster }} style={{ height: 49, width: 44 }}></Image>
                                        </View>
                                        <View style={{ flexDirection: "column", marginLeft: 14, width: wp("59%"), }}>
                                            <Text style={{ fontSize: 13 }}>{item.name}</Text>
                                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                                {/* <Image source={require("../../../../Assets/wishList.png")} style={{ height: 20, width: 20 }}></Image> */}
                                                <Text style={{ marginLeft: 10 }}>₹ {item.price}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <Icon

                                        name='chevron-right'
                                        size={17}
                                        color='black'
                                    />

                                </View>
                            </TouchableOpacity>
                        }

                    />
                    :

                    <FlatList
                        data={searchData}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => getDetail(item)}>
                                <View style={{
                                    width: wp("90%"), shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    elevation: 5,
                                    backgroundColor: "white",
                                    alignItems: "center",
                                    justifyContent: "space-between",
                                    marginBottom: 10,
                                    alignSelf: "center",
                                    marginTop: 10,
                                    flexDirection: "row",
                                    paddingLeft: 10, paddingRight: 10

                                }}>
                                    <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}>
                                        <View style={{ width: 49, alignItems: "center", justifyContent: "center" }}>
                                            <Image source={{ uri: item.poster }} style={{ height: 49, width: 44 }}></Image>
                                        </View>
                                        <View style={{ flexDirection: "column", marginLeft: 14, width: wp("59%"), }}>
                                            <Text style={{ fontSize: 13 }}>{item.name}</Text>
                                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                                {/* <Image source={require("../../../../Assets/wishList.png")} style={{ height: 20, width: 20 }}></Image>
                                                <Text style={{ marginLeft: 10 }}>{item.likeCount}</Text> */}
                                                <Text style={{ marginLeft: 10 }}>₹ {item.price}</Text>

                                            </View>
                                        </View>
                                    </View>
                                    <Icon

                                        name='chevron-right'
                                        size={17}
                                        color='black'
                                    />

                                </View>
                            </TouchableOpacity>
                        }

                    />}




            </ScrollView>
        </View >
    );
};

export default ProductPage;


