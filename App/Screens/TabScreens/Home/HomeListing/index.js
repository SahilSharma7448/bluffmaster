import React, { useEffect, useRef } from 'react';
import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, FlatList } from 'react-native';
import { SearchBar, Input } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../../../Utility"
import Icon from 'react-native-vector-icons/FontAwesome'
import { ScrollView } from 'react-native-gesture-handler';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch, useSelector } from 'react-redux';
import {
    getProducts,
    getProductDetail

} from '../../../../Redux/Action';
import { Alert } from 'react-native';
import Loader from '../../../../Constants/Loader';
import FastImage from 'react-native-fast-image';

const HomeListing = ({ navigation }) => {
    const dispatch = useDispatch()
    const bannerList = useSelector((state) => state.GetBanners);
    const CategoryList = useSelector((state) => state.GetCategories);
    const Recentdata = useSelector((state) => state.RecentData);

    const [searchText, setSearctText] = useState("")
    const ProductsImageRef = useRef("")
    const [activeSlide, setActiveSlide] = useState(0)
    const [loader, setLoader] = useState(false);


    // 57%



    const _renderItem = ({ item, index }) => {
        return (
            // <Image source={{ uri: item.image }} style={{ height: hp("30%"), width: wp("99%"), resizeMode: "contain" }}></Image>
            <FastImage
                source={{ uri: item.image }} style={{ height: hp("30%"), width: wp("99%"), }}
                resizeMode={FastImage.resizeMode.contain}
            />
        );
    }
    const getProductsFn = async (item) => {
        console.log("check selected item", item)
        setLoader(true)
        let res = await dispatch(getProducts(item.id))
        if (res.status == 1) {
            setLoader(false)
            console.log("check response of category", res)
            if (res.productArray.length !== 0) {
                navigation.navigate('ProductPage')

            }
            else {
                return Alert.alert('', 'No Product Found in this category');

            }
        }
        else {
            setLoader(false)
            return Alert.alert('', res.message);
        }
    }

    const getDetail = async (item) => {
        console.log("check product detail", item)
        await dispatch(getProductDetail(item))
        navigation.navigate('DetailPage')


    }
    return (
        <View>
            <Loader isLoading={loader}></Loader>

            <ScrollView>
                <View style={{ marginTop: 20 }}>

                    <Carousel
                        style={{ backgroundColor: "red" }}

                        containerCustomStyle={{ backgroundColor: "grey", alignSelf: "center", }}
                        // layout={'stack'} layoutCardOffset={`18`}
                        loop
                        useScrollView
                        autoplay
                        ref={ProductsImageRef}
                        data={bannerList}
                        renderItem={_renderItem}
                        sliderWidth={wp("100%")}
                        itemWidth={wp("99%")}
                        onSnapToItem={(index) => setActiveSlide(index)}
                    />
                    {/* <Pagination
                        dotsLength={bannerList.length}
                        activeDotIndex={activeSlide}
                        containerStyle={{ backgroundColor: 'grey' }}
                        dotStyle={{
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginHorizontal: 8,
                            backgroundColor: 'rgba(255, 255, 255, 0.92)'
                        }}
                        inactiveDotStyle={{
                            // Define styles for inactive dots here
                        }}
                        inactiveDotOpacity={0.4}
                        inactiveDotScale={0.6}
                    /> */}

                </View>
                <View style={{ marginTop: 40, width: wp("90%"), alignSelf: "center" }}>
                    <FlatList
                        data={CategoryList}
                        numColumns={3}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => getProductsFn(item)}>
                                <View style={{
                                    height: hp("15%"), width: wp("25%"), shadowColor: '#000',
                                    shadowOffset: { width: 0, height: 1 },
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    elevation: 5,
                                    backgroundColor: "white",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    marginBottom: 10,
                                    alignSelf: "center",
                                    marginLeft: wp(" 4%")
                                }}>
                                    <Image source={{ uri: item.image }} resizeMode={"cover"} style={{ height: 47, width: 47, alignSelf: "center", justifyContent: "center" }}></Image>
                                    <Text style={{ fontSize: 10, marginTop: 10 }}>{item.name}</Text>
                                </View>
                            </TouchableOpacity>
                        }

                    />
                </View>
                <Text style={{ marginLeft: wp(" 5%"), fontSize: 20, fontWeight: "600", marginTop: 20 }}>Recent Product</Text>

                <FlatList
                    data={Recentdata}
                    renderItem={({ item }) =>
                        <TouchableOpacity onPress={() => getDetail(item)}>
                            <View style={{
                                width: wp("90%"), shadowColor: '#000',
                                shadowOffset: { width: 0, height: 1 },
                                shadowOpacity: 0.8,
                                shadowRadius: 2,
                                elevation: 5,
                                backgroundColor: "white",
                                alignItems: "center",
                                justifyContent: "space-between",
                                marginBottom: 10,
                                alignSelf: "center",
                                marginTop: 10,
                                flexDirection: "row",
                                paddingLeft: 10, paddingRight: 10

                            }}>
                                <View style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}>
                                    <View style={{ width: 49, alignItems: "center", justifyContent: "center" }}>
                                        <Image source={{ uri: item.poster }} style={{ height: 49, width: 44 }}></Image>
                                    </View>
                                    <View style={{ flexDirection: "column", marginLeft: 14, width: wp("59%"), }}>
                                        <Text style={{ fontSize: 13 }}>{item.name}</Text>
                                        <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                            {/* <Image source={require("../../../Assets/wishList.png")} style={{ height: 20, width: 20 }}></Image> */}
                                            <Text style={{ marginLeft: 10 }}>₹ {item.price}</Text>
                                        </View>
                                    </View>
                                </View>
                                <Icon

                                    name='chevron-right'
                                    size={17}
                                    color='black'
                                />

                            </View>
                        </TouchableOpacity>
                    }

                />

            </ScrollView>
        </View >
    );
};

export default HomeListing;


