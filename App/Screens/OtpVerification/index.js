import React, { useEffect, useRef } from 'react';
import { useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'
import OtpInputs from 'react-native-otp-inputs';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../../Utility"
const OtpVerification = ({ navigation }) => {
    const otpInput = useRef(null);
    const clearText = () => {
        otpInput.current.clear();
    }

    const setText = () => {
        otpInput.current.setValue("1234");
    }
    // 57%
    return (
        <View>
            <ScrollView>
                <View style={{ height: hp("30%",), width: wp('100%'), backgroundColor: "black", alignItems: "center" }}>

                    <Image source={require("../../Assets/logo.png")} style={{ height: hp("20%"), width: wp("70%"), marginTop: 40 }}></Image>
                </View>
                <Text style={{ fontSize: 22, fontWeight: "600", textAlign: "center", marginTop: 30 }}>OTP Verification</Text>

                <View style={{ marginTop: 20, alignItems: "center" }}>




                    <OtpInputs
                        handleChange={(code) => console.log(code)}
                        numberOfInputs={4}
                        inputStyles={{ color: "white", textAlign: "center", fontWeight: "800" }}
                        inputContainerStyles={{ width: 57, backgroundColor: "black", marginLeft: 20 }}
                        underlineColorAndroid={"black"}
                        style={{ flexDirection: "row", alignSelf: "center", alignItems: "center", justifyContent: "space-between", marginTop: 40 }}
                    />


                    <Button
                        onPress={() => navigation.navigate("ResetPassword")}
                        buttonStyle={{ width: wp("90%"), height: hp("8%"), borderColor: "black", borderWidth: 1, alignSelf: "center", borderRadius: 10, marginTop: 90, backgroundColor: "black", marginBottom: 20 }}
                        // raised={true}
                        titleStyle={{ color: "white" }}
                        title="Submit"
                        type="solid"
                    />

                </View>
            </ScrollView>
        </View>
    );
};

export default OtpVerification;
