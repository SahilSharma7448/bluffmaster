// 123456 123456
import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet, Alert } from 'react-native';
import { Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from '../../Utility';
import * as Utility from '../../Utility';
import { useDispatch } from 'react-redux';
import {
    loginApi,
    getBanners,
    getCategroy
} from '../../Redux/Action';
import Loader from '../../Constants/Loader';
import Logo from "../../Constants/Logo"
import { TouchableOpacity } from 'react-native';
const Login = ({ navigation }) => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loader, setLoader] = useState(false);
    const loginFu = async () => {
        if (await Utility.isFieldEmpty(email && password)) {
            return Alert.alert('Please Fill All Fields');
        }

        else {
            //
            setLoader(true)
            // await dispatch(getBanners())
            // await dispatch(getCategroy())

            let res = await dispatch(loginApi(email.toLocaleLowerCase(), password.toLocaleLowerCase()))
            if (res.status == 1) {
                setLoader(false)

                console.log("check if")
                navigation.navigate('BottomTab');
            }
            else {
                console.log("check else")
                setLoader(false)

                return Alert.alert('', res.message);
            }
        }
    };
    // 57%
    return (
        <View>
            <ScrollView>
                <Loader isLoading={loader}></Loader>
                {/* <View style={styles.backGroundView}>
                    <Image
                        source={require('../../Assets/logo.png')}
                        style={styles.logoImg}></Image>
                </View> */}
                <Logo></Logo>
                <Text style={styles.welcomeTxt}>Welcome back!</Text>
                <View style={{ marginTop: 40 }}>
                    <Input
                        onChangeText={(text) => setEmail(text)}
                        placeholder="Your Email Adress"
                        leftIcon={<Icon name="envelope" size={17} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />

                    <Input
                        onChangeText={(text) => setPassword(text)}
                        secureTextEntry={true}
                        placeholder="Your Password"
                        leftIcon={<Icon name="lock" size={24} color="black" />}
                        containerStyle={styles.inputContainer}
                        style={{ fontSize: 14 }}
                    />
                    {/* <Button
                        onPress={() => loginFu()}
                        buttonStyle={styles.loginButton}
                        // raised={true}
                        titleStyle={{ color: 'black' }}
                        title="Login"
                        type="outline"
                    /> */}
                    <TouchableOpacity onPress={() => loginFu()} activeOpacity={1}>
                        <View style={styles.loginButton}>
                            <Text style={{ fontSize: 18, fontWeight: "700" }}>Login </Text>
                        </View>
                    </TouchableOpacity>
                    <Button
                        onPress={() => navigation.navigate('SignUp')}
                        buttonStyle={styles.signUpButton}
                        // raised={true}
                        titleStyle={{ color: 'white' }}
                        title="Register"
                        type="solid"

                    />
                    <Text
                        style={styles.forgotTxt}
                        onPress={() => navigation.navigate('ForgotPassword')}>
                        Forgot password?
          </Text>
                </View>
            </ScrollView>
        </View>
    );
};

export default Login;
const styles = StyleSheet.create({
    backGroundView: {
        height: hp('30%'),
        width: wp('100%'),
        backgroundColor: 'black',
        alignItems: 'center',
    },
    logoImg: { height: hp('20%'), width: wp('70%'), marginTop: 40 },
    welcomeTxt: {
        fontSize: 22,
        fontWeight: '600',
        textAlign: 'center',
        marginTop: 30,
    },
    inputContainer: { width: wp('90%'), alignSelf: 'center' },
    loginButton: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        alignItems: "center",
        justifyContent: "center"
    },
    signUpButton: {
        width: wp('90%'),
        height: hp('8%'),
        borderColor: 'black',
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: 10,
        marginTop: 20,
        backgroundColor: 'black',
    },
    forgotTxt: {
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'center',
        marginTop: 20,
    },
});

