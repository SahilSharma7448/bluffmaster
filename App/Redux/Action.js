
import * as Services from '../api/services';
import * as url from '../api/url';
import * as Utility from '../Utility/index';
import * as Const from './Const';
import { Alert } from 'react-native';

export const loginApi = (email, password) => async (dispatch) => {
    let body = {
        email: email,
        password: password
    }
    let res = await Services.post(url.LOGIN, '', body);
    console.log('response of login', res);
    await Utility.setInLocalStorge('token', res.token)
    return res;

};
export const registerApi = (firstname, lastname, email, password, phone) => async (dispatch) => {
    let body = {
        firstname: firstname,
        lastname: lastname,
        email: email,
        password: password,
        phone_no: phone
    }
    let res = await Services.post(url.REGISTER, '', body);

    console.log('response of login', res);

    return res;

}
export const forgotPasswordApi = (email) => async (dispatch) => {
    let body = {

        email: email,
    }
    let res = await Services.post(url.FORGOTPASSWORD, '', body);

    console.log('response of login', res);

    return res;

}
export const getOwnProfile = () => async (dispatch) => {
    let token = await Utility.getFromLocalStorge('token')
    let res = await Services.FormDataPost(url.GETOWNPROFILE, token);
    console.log("check my data", res)
    dispatch({
        type: Const.MyProfileData,
        payload: res,
    });
    return res
}
export const changePasswordApi = (oldPass, NewPass) => async () => {
    let token = await Utility.getFromLocalStorge('token')
    let body = {
        old_password: oldPass,
        new_password: NewPass,

    }
    console.log("check body of change passowrd", body)
    let res = await Services.post(url.CHANGEPASSWORD, token, body);
    console.log("check my data", res)
    return res;

}
export const getBanners = () => async (dispatch) => {
    // let token = await Utility.getFromLocalStorge('token')

    let res = await Services.post(url.GETBANNERS);
    console.log("check banner response", res)
    dispatch({
        type: Const.GetBanners,
        payload: res.bannerArr,
    });
    return res;
}
export const getCategroy = () => async (dispatch) => {
    // let token = await Utility.getFromLocalStorge('token')

    let res = await Services.post(url.GETBANNERS);
    console.log("check banner response", res)
    dispatch({
        type: Const.GetCategories,
        payload: res.catArray,
    });
    return res;
}

export const getProducts = (id) => async (dispatch) => {
    let token = await Utility.getFromLocalStorge('token')
    let body = { category_id: id }
    let res = await Services.post(url.GetProducts, token, body);
    console.log("check banner response", res)
    dispatch({
        type: Const.GetProducts,
        payload: res.productArray,
    });
    return res;
}

export const getProductDetail = (item) => async (dispatch) => {
    await console.log("check banner response", item)

    dispatch({
        type: Const.GetProductDetail,
        payload: item,
    });
    return true
}

export const getRecentData = () => async (dispatch) => {
    let token = 'eyJpdiI6IkpyY0FOSlRIUGNHNk9PTHIxTURzakE9PSIsInZhbHVlIjoiU25RREpWKzlIallYc3pad1FpYy93UT09IiwibWFjIjoiYjBkZDI0OTUxNTI5ZWQ2MzgwNWVhYWRlMjQ3MmRjMDMxYmViZjA3YmRlMGQ3OGY2MTI0MjU5YWIzZGM5MjRjNSJ9';

    let res = await Services.post(url.GetRecentProduct, token);
    console.log("check banner response", res)
    dispatch({
        type: Const.RecentData,
        payload: res.productArray,
    });
    return res;
}
export const searchProduct = (searchName) => async (dispatch) => {
    let token = await Utility.getFromLocalStorge('token')
    let body = {
        search: searchName
    }
    let res = await Services.post(url.SEARCHPRODUCT, token, body);
    dispatch({
        type: Const.SearchProductData,
        payload: res.productArray,
    });
    return res

}
export const likeUnlike = (id) => async () => {
    let token = await Utility.getFromLocalStorge('token')
    let body = {
        product_id: id
    }
    let res = await Services.post(url.LIKEDISLIKE, token, body);
    console.log("check response", res);
}
export const wishListData = () => async (dispatch) => {
    let token = await Utility.getFromLocalStorge('token')
    let res = await Services.post(url.FAVROUTIES, token);
    dispatch({
        type: Const.FavrioutesList,
        payload: res.productArray,
    });
    return
}