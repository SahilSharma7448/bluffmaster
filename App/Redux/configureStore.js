
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducer';
import thunk from 'redux-thunk';
export default function configureStore() {
    let store = createStore(rootReducer, applyMiddleware(thunk))
    return store
}

//////////// REDUX CONST/////////////

export const Login_Data = 'Login_Data';

