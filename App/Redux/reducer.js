
import {
    GetBanners,
    Login_Data,
    MyProfileData,
    GetCategories,
    GetProducts,
    GetProductDetail,
    RecentData,
    SearchProductData,
    FavrioutesList
} from './Const';


const initialState = {
    Logindata: [],
    MyProfileData: {},
    GetBanners: [],
    GetCategories: [],
    GetProducts: [],
    GetProductDetail: {},
    RecentData: [],
    SearchProductData: [],
    FavrioutesList: []

};


export default function (state = initialState, { type, payload }) {
    switch (type) {
        case Login_Data:
            return {
                ...state,
                Logindata: payload,
            };
        case MyProfileData:
            return {
                ...state,
                MyProfileData: payload,
            };
        case GetBanners:
            return {
                ...state,
                GetBanners: payload,
            };
        case GetCategories:
            return {
                ...state,
                GetCategories: payload,
            };
        case GetProducts:
            return {
                ...state,
                GetProducts: payload,
            };
        case GetProductDetail:
            return {
                ...state,
                GetProductDetail: payload,
            };
        case RecentData:
            return {
                ...state,
                RecentData: payload,
            };
        case SearchProductData:
            return {
                ...state,
                SearchProductData: payload,
            };
        case FavrioutesList:
            return {
                ...state,
                FavrioutesList: payload,
            };
        default: {
            return state;
        }
    }
}
