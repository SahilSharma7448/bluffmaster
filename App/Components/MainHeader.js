import React, { useEffect, useRef } from 'react';
import { useState } from 'react';
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Keyboard, FlatList } from 'react-native';
import { SearchBar, Input } from 'react-native-elements';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "../Utility"
import Icon from 'react-native-vector-icons/FontAwesome'
const MainHeader = ({ navigation, route, name }) => {

    // 57%
    return (
        <View style={{ flexDirection: "row", justifyContent: "space-between", height: 70, backgroundColor: "black", width: wp(" 100%"), alignItems: "center" }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Image source={require("../Assets/back.png")} style={{ height: 17, width: 24, marginLeft: 10 }}></Image>
            </TouchableOpacity>
            <Text style={{ color: "white", fontWeight: "600" }}>{name}</Text>
            <Text style={{ color: "black", fontWeight: "600" }}>kjegg</Text>

        </View >
    );
};

export default MainHeader;


