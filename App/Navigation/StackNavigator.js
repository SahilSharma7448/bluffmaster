import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from '../Screens/Splash';
import Login from "../Screens/Login"
import SignUp from "../Screens/SignUp"
import ForgotPassword from "../Screens/ForgotPassword"
import OtpVerification from "../Screens/OtpVerification"
import ResetPassword from "../Screens/ResetPassword"
// import HomeListing from "../Screens/TabScreens/Home/HomeListing"
import BottomTab from "./BottomTab"
import DetailPage from "../Screens/TabScreens/Home/DetailPage"
import ProductPage from "../Screens/TabScreens/Home/ProductPage"
import PersonalAccount from "../Screens/TabScreens/Account/PersonalAccount"
import ChangePassword from "../Screens/TabScreens/Account/ChangePassword"
import ContactUs from "../Screens/TabScreens/Account/ContactUs"
import PrivacyPolicy from "../Screens/TabScreens/Account/PrivacyPolicy"
/////// Punjabi /////

const Stack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="OtpVerification"
        component={OtpVerification}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{ headerShown: false }}
      />
      {/* <Stack.Screen
        name="HomeListing"
        component={HomeListing}
        options={{ headerShown: false }}
      /> */}
      <Stack.Screen
        name="BottomTab"
        component={BottomTab}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DetailPage"
        component={DetailPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ProductPage"
        component={ProductPage}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PersonalAccount"
        component={PersonalAccount}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="PrivacyPolicy"
        component={PrivacyPolicy}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export { MainStackNavigator };
