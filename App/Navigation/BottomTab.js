import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet } from 'react-native';
import HomeListing from "../Screens/TabScreens/Home/HomeListing"
import WishList from "../Screens/TabScreens/WishList"
import AccountHome from "../Screens/TabScreens/Account/AccountHome"
import Icon from 'react-native-vector-icons/FontAwesome'
import { useDispatch, useSelector } from 'react-redux';
import {
    wishListData
} from '../Redux/Action';
const Tab = createBottomTabNavigator();
const BottomTab = () => {
    const dispatch = useDispatch();
    return (
        <Tab.Navigator tabBarOptions={{ activeTintColor: "black" }}>
            <Tab.Screen

                name="Browse"
                component={HomeListing}

                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='navicon'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />

            <Tab.Screen
                name="Favorities"
                component={WishList}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='heart'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                        dispatch(wishListData());
                    },
                })}
            />
            <Tab.Screen
                name="Profile"
                component={AccountHome}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='user'
                            size={24}
                            color={focused ? "black" : '#d9d9d9'}
                        />
                    ),
                })}
            />
        </Tab.Navigator>
    );
};

export default BottomTab;
const styles = StyleSheet.create({

    tabImg2: {
        alignSelf: 'center',
        tintColor: 'grey',
        height: 25,
        width: 30,
    },
    selectedTabImg2: {
        alignSelf: 'center',
        tintColor: 'black',
        height: 25,
        width: 30,
    },

});
