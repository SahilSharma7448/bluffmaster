// import React from 'react';
// import {NavigationContainer} from '@react-navigation/native';

// import {MainStackNavigator} from './App/Navigation/StackNavigator';

// const App = () => {
//   return (
//     <NavigationContainer>
//       <MainStackNavigator />
//     </NavigationContainer>
//   );
// };
// export default App;
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { MainStackNavigator } from './App/Navigation/StackNavigator';
import { Provider } from 'react-redux'
import configureStore from './App/Redux/configureStore';
const store = configureStore()
const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <MainStackNavigator></MainStackNavigator>
      </NavigationContainer>
    </Provider>
  )
}
export default App;